# README #

This is an easy to run Apache Trinidad demo. It demonstrates how to setup and use Trinidad 2.1.0 components with JSF 2.2 and CDI beans.

It uses gradle and jetty plugin.


## How do I get set up? ##

To build and run just type:

    ./gradlew jettyRun
